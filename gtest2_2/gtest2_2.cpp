// gtest2_2.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\Laba2\time_diag1.h"
#include "gtest\gtest.h"
#include <cmath>
#include <sstream>
using namespace std;

TEST(TestClassDiag, TestConstruct2)
{
	time_diag1 testD('0', 60);
	//expect no throw
	ASSERT_NO_THROW(time_diag1 testD_('0', 60));
	EXPECT_EQ(60, testD.Get_b_d());
	EXPECT_EQ(60, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ(60, testD.Get_max_d());
	
}

TEST(TestClassDiag, TestConstruct2_1)
{
	EXPECT_THROW(time_diag1 testD('0', 70), const char*);
	try{
		time_diag1 testD_('0', 70);
	}
	catch (const char* error){
		EXPECT_STREQ("Too big duration!!!", error);
	}
}

TEST(TestClassDiag, TestConstruct2_2)
{
	EXPECT_THROW(time_diag1 testD(0, -5), const char*);
	try{
		time_diag1 testD_(0, -5);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
}

TEST(TestClassDiag, TestConstruct1)
{
	time_diag1 testD;
	ASSERT_NO_THROW(time_diag1 testD_);
	EXPECT_EQ(0, testD.Get_b_d());
	EXPECT_EQ(0, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(NULL, testD.Get_mas());
}

TEST(TestClassDiag, TestConstruct3)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	EXPECT_EQ(17, testD.Get_b_d());
	EXPECT_EQ(11, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(5, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(2, testD.Get_dur(4));
	EXPECT_EQ(1, testD.Get_dur(5));
	EXPECT_EQ(1, testD.Get_dur(6));
	EXPECT_EQ(1, testD.Get_dur(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ(1, testD.Get_dur(9));
	EXPECT_EQ(1, testD.Get_dur(10));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	EXPECT_EQ('1', testD.Get_val(5));
	EXPECT_EQ('0', testD.Get_val(6));
	EXPECT_EQ('1', testD.Get_val(7));
	EXPECT_EQ('0', testD.Get_val(8));
	EXPECT_EQ('x', testD.Get_val(9));
	EXPECT_EQ('1', testD.Get_val(10));

}

TEST(TestClassDiag, TestConstruct3_1)
{
	char str[18] = "\0";
	EXPECT_THROW(time_diag1 testD(str), const char*);
	try{
		time_diag1 testD_(str);
	}
	catch (const char* error){
		EXPECT_STREQ("Null string!!!", error);
	}
}

TEST(TestClassDiag, TestConstruct3_2)
{
	char str[62] = "qwertyuioplkjhgfdsazxcvbnmlkjhgfdsaqwertyuioplkjhgfdsazxcvbnm";
	EXPECT_THROW(time_diag1 testD(str), const char*);
	try{
		time_diag1 testD_(str);
	}
	catch (const char* error){
		EXPECT_STREQ("Overflow time diagram!!!", error);
	}
}

TEST(TestClassDiag, TestConstruct4)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD1(testD);
	ASSERT_NO_THROW(time_diag1 testD1_(testD));


	EXPECT_EQ(17, testD1.Get_b_d());
	EXPECT_EQ(11, testD1.Get_b_v());
	EXPECT_EQ(60, testD1.Get_max_d());
	EXPECT_EQ(1, testD1.Get_dur(0));
	EXPECT_EQ(1, testD1.Get_dur(1));
	EXPECT_EQ(5, testD1.Get_dur(2));
	EXPECT_EQ(2, testD1.Get_dur(3));
	EXPECT_EQ(2, testD1.Get_dur(4));
	EXPECT_EQ(1, testD1.Get_dur(5));
	EXPECT_EQ(1, testD1.Get_dur(6));
	EXPECT_EQ(1, testD1.Get_dur(7));
	EXPECT_EQ(1, testD1.Get_dur(8));
	EXPECT_EQ(1, testD1.Get_dur(9));
	EXPECT_EQ(1, testD1.Get_dur(10));
	EXPECT_EQ('0', testD1.Get_val(0));
	EXPECT_EQ('1', testD1.Get_val(1));
	EXPECT_EQ('x', testD1.Get_val(2));
	EXPECT_EQ('1', testD1.Get_val(3));
	EXPECT_EQ('0', testD1.Get_val(4));
	EXPECT_EQ('1', testD1.Get_val(5));
	EXPECT_EQ('0', testD1.Get_val(6));
	EXPECT_EQ('1', testD1.Get_val(7));
	EXPECT_EQ('0', testD1.Get_val(8));
	EXPECT_EQ('x', testD1.Get_val(9));
	EXPECT_EQ('1', testD1.Get_val(10));

}

TEST(TestClassDiag, TestConstructSet)
{
	time_diag1 testD, testD_;
	testD.Set('1', 4, 0);
	ASSERT_NO_THROW(testD_.Set('1', 4, 0));
	EXPECT_EQ(4, testD.Get_b_d());
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(4, testD.Get_dur(0));
	EXPECT_EQ('1', testD.Get_val(0));
}

TEST(TestClassDiag, TestConstructSet_1)
{
	time_diag1 testD, testD_;
	EXPECT_THROW(testD.Set('1', 65, 0), const char*);
	try{
		testD_.Set('1', 65, 0);
	}
	catch (const char* error){
		EXPECT_STREQ("Too big duration!!!", error);
	}
	EXPECT_THROW(testD.Set('1', -1, 0), const char*);
	try{
		testD_.Set('1', -1, 0);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD.Set('1', 5, -3), const char*);
	try{
		testD_.Set('1', 5, -3);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD.Set('1', 5, 2), const char*);
	try{
		testD_.Set('1', 5, 2);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
}

TEST(TestClassDiag, TestCorrect)
{
	time_diag1 testD, testD_;
	testD.Set('0', 2, 0);
	ASSERT_NO_THROW(testD_.Set('0', 2, 0));
	testD.Set('1', 3, 1);
	ASSERT_NO_THROW(testD_.Set('1', 3, 1));
	testD.Set('1', 2, 2);
	ASSERT_NO_THROW(testD_.Set('1', 2, 2));
	testD.Set('x', 2, 2);
	ASSERT_NO_THROW(testD_.Set('x', 2, 2));
	testD.Set('x', 4, 3);
	ASSERT_NO_THROW(testD_.Set('x', 4, 3));

	EXPECT_EQ(13, testD.Get_b_d());
	EXPECT_EQ(3, testD.Get_b_v());
	EXPECT_EQ(2, testD.Get_dur(0));
	EXPECT_EQ(5, testD.Get_dur(1));
	EXPECT_EQ(6, testD.Get_dur(2));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));


}

TEST(TestClassDiag, TestUnion)
{
	char str1[8] = "abcdefg";
	char str2[9] = "klmnoprs";
	char str3[8] = "1011011";
	char str4[9] = "10110100";
	time_diag1 testD1(str1);
	ASSERT_NO_THROW(time_diag1 testD1_(str1));
	time_diag1 testD2(str2);
	ASSERT_NO_THROW(time_diag1 testD2_(str1));
	time_diag1 testD3(str3);
	ASSERT_NO_THROW(time_diag1 testD3_(str1));
	time_diag1 testD4(str4);
	ASSERT_NO_THROW(time_diag1 testD4_(str1));
	time_diag1 testD, testDd;
	time_diag1 testD_, testDd_;
	
	testD = testD1 + testD2;
	ASSERT_NO_THROW(testDd = testD1 + testD2);
	testD_ = testD3 + testD4;
	ASSERT_NO_THROW(testDd_ = testD3 + testD4);

	EXPECT_EQ(15, testD.Get_b_d());
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ(15, testD.Get_dur(0));
	EXPECT_EQ('x', testD.Get_val(0));

	EXPECT_EQ(15, testD_.Get_b_d());
	EXPECT_EQ(10, testD_.Get_b_v());
	EXPECT_EQ(1, testD_.Get_dur(0));
	EXPECT_EQ(1, testD_.Get_dur(1));
	EXPECT_EQ(2, testD_.Get_dur(2));
	EXPECT_EQ(1, testD_.Get_dur(3));
	EXPECT_EQ(3, testD_.Get_dur(4));
	EXPECT_EQ(1, testD_.Get_dur(5));
	EXPECT_EQ(2, testD_.Get_dur(6));
	EXPECT_EQ(1, testD_.Get_dur(7));
	EXPECT_EQ(1, testD_.Get_dur(8));
	EXPECT_EQ(2, testD_.Get_dur(9));
	EXPECT_EQ('1', testD_.Get_val(0));
	EXPECT_EQ('0', testD_.Get_val(1));
	EXPECT_EQ('1', testD_.Get_val(2));
	EXPECT_EQ('0', testD_.Get_val(3));
	EXPECT_EQ('1', testD_.Get_val(4));
	EXPECT_EQ('0', testD_.Get_val(5));
	EXPECT_EQ('1', testD_.Get_val(6));
	EXPECT_EQ('0', testD_.Get_val(7));
	EXPECT_EQ('1', testD_.Get_val(8));
	EXPECT_EQ('0', testD_.Get_val(9));

}

TEST(TestClassDiag, TestUnion_1)
{
	char str1[35] = "abcdefgqawesrdcfvghbzsxdcfvgbsexrd";
	char str2[37] = "klmnoprsxdcfvghbjjhgfcghkjhghjgchhgt";

	time_diag1 testD1(str1);
	ASSERT_NO_THROW(time_diag1 testD1_(str1));
	time_diag1 testD2(str2);
	ASSERT_NO_THROW(time_diag1 testD2_(str2));
	time_diag1 testD, testD_;
	
	EXPECT_THROW(testD = testD1 + testD2, const char*);
	try{
		testD_ = testD1 + testD2;
	}
	catch (const char* error){
		EXPECT_STREQ("Overflow time diagram!!!", error);
	}

}

TEST(TestClassDiag, TestReplace)
{
	char str1[8] = "abcdefg";
	char str2[4] = "klm";
	char str3[9] = "11111000";
	char str4[4] = "110";
	time_diag1 testD1(str1);
	ASSERT_NO_THROW(time_diag1 testD1_(str1));
	time_diag1 testD2(str2);
	ASSERT_NO_THROW(time_diag1 testD2_(str2));
	time_diag1 testD3(str3);
	ASSERT_NO_THROW(time_diag1 testD3_(str3));
	time_diag1 testD4(str4);
	ASSERT_NO_THROW(time_diag1 testD4_(str4));

	time_diag1 testD1_(str1);
	time_diag1 testD2_(str2);
	time_diag1 testD3_(str3);
	time_diag1 testD4_(str4);
	testD1(testD2, 3);
	ASSERT_NO_THROW(testD1_(testD2_, 3));
	testD3(testD4, 0);
	ASSERT_NO_THROW(testD3_(testD4_, 0));

	EXPECT_EQ(7, testD1.Get_b_d());
	EXPECT_EQ(1, testD1.Get_b_v());
	EXPECT_EQ(7, testD1.Get_dur(0));
	EXPECT_EQ('x', testD1.Get_val(0));


	EXPECT_EQ(8, testD3.Get_b_d());
	EXPECT_EQ(4, testD3.Get_b_v());
	EXPECT_EQ(2, testD3.Get_dur(0));
	EXPECT_EQ(1, testD3.Get_dur(1));
	EXPECT_EQ(2, testD3.Get_dur(2));
	EXPECT_EQ(3, testD3.Get_dur(3));
	EXPECT_EQ('1', testD3.Get_val(0));
	EXPECT_EQ('0', testD3.Get_val(1));
	EXPECT_EQ('1', testD3.Get_val(2));
	EXPECT_EQ('0', testD3.Get_val(3));


}

TEST(TestClassDiag, TestReplace_1)
{
	char str1[30] = "10101010101010101010101010101";
	char str2[20] = "1010101010101010101";
	char str3[45] = "111110004g5s46g84es4g8es4g8e64g5444411111115";
	char str4[25] = "11010110fsd1fdsf1010v1se";
	time_diag1 testD1(str1);
	ASSERT_NO_THROW(time_diag1 testD1_(str1));
	time_diag1 testD2(str2);
	ASSERT_NO_THROW(time_diag1 testD2_(str2));
	time_diag1 testD3(str3);
	ASSERT_NO_THROW(time_diag1 testD3_(str3));
	time_diag1 testD4(str4);
	ASSERT_NO_THROW(time_diag1 testD4_(str4));
	time_diag1 testD1_(str1);
	time_diag1 testD2_(str2);
	time_diag1 testD3_(str3);
	time_diag1 testD4_(str4);
	EXPECT_THROW(testD1(testD2, -3), const char*);
	try{
		testD1_(testD2_, 3);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD1(testD2, 31), const char*);
	try{
		testD1_(testD2_, 31);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD3(testD4, 40), const char*);
	try{
		testD3_(testD4_, 40);
	}
	catch (const char* error){
		EXPECT_STREQ("Overflow time diagram!!!", error);
	}

}

TEST(TestClassDiag, TestCopy)
{
	char str[12] = "10110100110";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_(str);
	testD.copy(3);
	ASSERT_NO_THROW(testD_.copy(3));
	EXPECT_EQ(44, testD.Get_b_d());
	EXPECT_EQ(32, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(2, testD.Get_dur(2));
	EXPECT_EQ(1, testD.Get_dur(3));
	EXPECT_EQ(1, testD.Get_dur(4));
	EXPECT_EQ(2, testD.Get_dur(5));
	EXPECT_EQ(2, testD.Get_dur(6));
	EXPECT_EQ(1, testD.Get_dur(7));
	EXPECT_EQ('1', testD.Get_val(0));
	EXPECT_EQ('0', testD.Get_val(1));
	EXPECT_EQ('1', testD.Get_val(2));
	EXPECT_EQ('0', testD.Get_val(3));
	EXPECT_EQ('1', testD.Get_val(4));
	EXPECT_EQ('0', testD.Get_val(5));
	EXPECT_EQ('1', testD.Get_val(6));
	EXPECT_EQ('0', testD.Get_val(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ('1', testD.Get_val(8));
}

TEST(TestClassDiag, TestCopy_1)
{
	char str[9] = "10110100";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_(str);
	EXPECT_THROW(testD.copy(-2), const char*);
	try{
		testD_.copy(-2);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD.copy(8), const char*);
	try{
		testD_.copy(8);
	}
	catch (const char* error){
		EXPECT_STREQ("Overflow time diagram!!!", error);
	}
	
	
}

TEST(TestClassDiag, TestShR)
{
	char str[12] = "10110100110";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_(str);
	testD.shift_right(5);
	ASSERT_NO_THROW(testD_.shift_right(5));
	EXPECT_EQ(16, testD.Get_b_d());
	EXPECT_EQ(9, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(5, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(1, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(1, testD.Get_dur(4));
	EXPECT_EQ(1, testD.Get_dur(5));
	EXPECT_EQ(2, testD.Get_dur(6));
	EXPECT_EQ(2, testD.Get_dur(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ('x', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('0', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	EXPECT_EQ('1', testD.Get_val(5));
	EXPECT_EQ('0', testD.Get_val(6));
	EXPECT_EQ('1', testD.Get_val(7));
	EXPECT_EQ('0', testD.Get_val(8));

}

TEST(TestClassDiag, TestShR_1)
{
	char str[12] = "10110100110";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_(str);
	char str1[41] = "1010101010101010101010101010101010101010";
	time_diag1 testD1(str1);
	ASSERT_NO_THROW(time_diag1 testD1_(str));
	time_diag1 testD1_(str);
	EXPECT_THROW(testD.shift_right(-2), const char*);
	try{
		testD_.shift_right(-2);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD.shift_right(52), const char*);
	try{
		testD1_.shift_right(52);
	}
	catch (const char* error){
		EXPECT_STREQ("Overflow time diagram!!!", error);
	}
	
	
}

TEST(TestClassDiag, TestShL)
{
	char str[12] = "10110100110";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD1(str));
	time_diag1 testD_;
	testD_ = testD.shift_left(5);
	ASSERT_NO_THROW(time_diag1 testD1_(str));

	EXPECT_EQ(6, testD_.Get_b_d());
	EXPECT_EQ(4, testD_.Get_b_v());
	EXPECT_EQ(60, testD_.Get_max_d());
	EXPECT_EQ(1, testD_.Get_dur(0));
	EXPECT_EQ(2, testD_.Get_dur(1));
	EXPECT_EQ(2, testD_.Get_dur(2));
	EXPECT_EQ(1, testD_.Get_dur(3));

	EXPECT_EQ('1', testD_.Get_val(0));
	EXPECT_EQ('0', testD_.Get_val(1));
	EXPECT_EQ('1', testD_.Get_val(2));
	EXPECT_EQ('0', testD_.Get_val(3));

}

TEST(TestClassDiag, TestShL_1)
{
	char str[12] = "10110100110";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_, testD1;

	EXPECT_THROW(testD_ = testD.shift_left(-2), const char*);
	try{
		testD1 = testD.shift_right(-2);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
	EXPECT_THROW(testD_ = testD.shift_left(62), const char*);
	try{
		testD1 = testD.shift_left(62);
	}
	catch (const char* error){
		EXPECT_STREQ("Incorrect data!!!", error);
	}
}

TEST(TestClassDiag, TestInput)
{
	time_diag1 testD,testD_;
	char *str = "10\n01\n11\nx1\nr1\nt1\n61\nr1\n11\n11\n01\n01\n11\n01\n11\n01\nx1\n11\n";
	char *str1 = "10\n01\n1-2\nx1\nr1\nt1\n61\nr1\n11\n11\n01\n01\n11\n01\n11\n01\nx1\n11\n";
	istringstream ss(str);
	ss >> testD;
	EXPECT_EQ(10, testD.Get_b_d());
	EXPECT_EQ(5, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(5, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(1, testD.Get_dur(4));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	istringstream ss1(str1);
	ss1 >> testD_;
	EXPECT_TRUE(ss1.fail());
	//�������������� ��������� ������
}

TEST(TestClassDiag, TestReInitCopy)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag1 testD(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD_, testD1;

	testD_= testD;
	ASSERT_NO_THROW(testD1 = testD);
	EXPECT_EQ(17, testD_.Get_b_d());
	EXPECT_EQ(11, testD_.Get_b_v());
	EXPECT_EQ(60, testD_.Get_max_d());
	EXPECT_EQ(1, testD_.Get_dur(0));
	EXPECT_EQ(1, testD_.Get_dur(1));
	EXPECT_EQ(5, testD_.Get_dur(2));
	EXPECT_EQ(2, testD_.Get_dur(3));
	EXPECT_EQ(2, testD_.Get_dur(4));
	EXPECT_EQ(1, testD_.Get_dur(5));
	EXPECT_EQ(1, testD_.Get_dur(6));
	EXPECT_EQ(1, testD_.Get_dur(7));
	EXPECT_EQ(1, testD_.Get_dur(8));
	EXPECT_EQ(1, testD_.Get_dur(9));
	EXPECT_EQ(1, testD_.Get_dur(10));
	EXPECT_EQ('0', testD_.Get_val(0));
	EXPECT_EQ('1', testD_.Get_val(1));
	EXPECT_EQ('x', testD_.Get_val(2));
	EXPECT_EQ('1', testD_.Get_val(3));
	EXPECT_EQ('0', testD_.Get_val(4));
	EXPECT_EQ('1', testD_.Get_val(5));
	EXPECT_EQ('0', testD_.Get_val(6));
	EXPECT_EQ('1', testD_.Get_val(7));
	EXPECT_EQ('0', testD_.Get_val(8));
	EXPECT_EQ('x', testD_.Get_val(9));
	EXPECT_EQ('1', testD_.Get_val(10));

}

TEST(TestClassDiag, TestMoveConstruct)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag1 testD1(str);
	time_diag1 testD2(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD = testD1 + testD2;	
}

TEST(TestClassDiag, TestReInitMove)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag1 testD1(str);
	time_diag1 testD2(str);
	ASSERT_NO_THROW(time_diag1 testD_(str));
	time_diag1 testD, testD_;

	testD = testD1 + testD2;
	ASSERT_NO_THROW(testD_ = testD1 + testD2);

}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
