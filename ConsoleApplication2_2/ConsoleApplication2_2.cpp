// ConsoleApplication2_2.cpp: ���������� ����� ����� ��� ����������� ����������.

#include "stdafx.h"
#include "..\Laba2\time_diag1.h"
#include <iostream>
#include <sstream>
#include <iomanip> 
#include <string.h>
using namespace std;

int getCh(char&);
int getNum(int &);
int Set(time_diag1&);
int Get_val(time_diag1&);
int Get_dur(time_diag1&);
int Get_busyv(time_diag1& td);
int Get_busyd(time_diag1& td);
int Get_maxd(time_diag1& td);
int Input(time_diag1&);
int Print(time_diag1&);
int Union(time_diag1&);
int Replace(time_diag1&);
int Copy(time_diag1&);
int Shift_r(time_diag1&);
int Shift_l(time_diag1&);
int Exit(time_diag1&);

int main()
{
	// ��� ������� ���� � �������
	std::locale loc("Russian");
	std::locale::global(loc);

	time_diag1 td;
	int i;
	int exit = 0;
	int ch = 0;


	char *msgs[] = { "- ���� ���������� �������\n", "- ����� �������� �������\n", "- ����� ������������ �������\n", "- ����� ���������� ������� �����\n", "- ����� ������� ������������\n",  "- ����� ������������ ������������\n", "- ���� ����������� ������\n", "- ����� ������������������ �������\n", "- ����������� ��������� ��������� � ������\n", "- ������ ��������� ��������� ������ � ������������ ������\n", "- ����������� ��������� ��������� ������������ ���������� ���\n", "- ����� ��������� ��������� ������\n", "- ����� ��������� ��������� �����\n", "- �����\n" };
	const int amount = sizeof(msgs) / sizeof(msgs[0]);
	for (i = 0; i<amount; ++i){
		printf("%d %s", i, msgs[i]);
	}
	int(*func[amount])(time_diag1& td) = { Set, Get_val, Get_dur, Get_busyv, Get_busyd, Get_maxd, Input, Print, Union, Replace, Copy, Shift_r, Shift_l, Exit };

	while (!exit){
		printf("\n�������� ����� ");
		scanf_s("%d", &ch);
		if ((ch>-1) && (ch<amount)){
			exit = (*func[ch])(td);
		}
	}

	return 0;
}

int getNum(int &a)
{
	cin >> a;
	if (!cin.good()){
		if (cin.eof()){
			cout << endl;
			cout << "����� ����� ��� ���������!!!" << endl;
			return 1;
		}
		if (cin.fail()){
			cout << endl;
			cin.clear();
			char c;
			cin >> c;
			return 2;
		}
		cout << "������������ ������ ���� �������!!!" << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		return -1;
	}
	return 0;
}

int getCh(char &c)
{
	cin >> c;
	if (cin.eof()){
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cout << endl;
		cout << "����� ����� ��� ���������!!!" << endl;
		return 1;
	}
	return 0;
}

int Set(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int ind, dur;
	char val;


	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ������ �������� �������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(ind)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);
	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� �������� �������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getCh(val)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);
	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ������������ �������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(dur)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		td.Set(val, dur, ind);
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	return 0;
}

int Get_val(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int ind;


	pr = "";
	fl = 0;
	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ������ �������� �������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(ind)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		cout << "�������� ������� = " << td.Get_val(ind) << endl;
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	return 0;
}

int Get_dur(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int ind;


	pr = "";
	fl = 0;
	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ������ �������� �������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(ind)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		cout << "������������ ������� = " << td.Get_dur(ind) << endl;
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	return 0;
}

int Get_busyv(time_diag1& td)
{
	cout << "���-�� ������� ����� = " << td.Get_b_v() << endl;
	return 0;
}

int Get_busyd(time_diag1& td)
{
	cout << "���-�� ������� ������������ = " << td.Get_b_d() << endl;
	return 0;
}

int Get_maxd(time_diag1& td)
{
	cout << "������������ ������������ ��������� ��������� = " << td.Get_max_d() << endl;
	return 0;
}

int Input(time_diag1& td)
{

	cin >> td;

	if (!cin.good()){
		if (!cin.eof())
			cout << "Error! Incorrect data!!!" << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
	}

	return 0;
}

int Print(time_diag1& td)
{
	cout << "��������� ���������: " << endl;
	cout << td;
	return 0;
}

int Union(time_diag1& td)
{
	char str[18] = "01xrt6r11001010x1";


	time_diag1 td1(str);

	try{
		td = td + td1;
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	cout << "���������: " << endl;
	cout << td;

	return 0;
}

int Replace(time_diag1& td)
{
	char str[18] = "01xrt6r11001010x1";


	time_diag1 td1(str);

	try{
		td(td1, 5);
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	cout << "���������: " << endl;
	cout << td;

	return 0;
}

int Copy(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int k;


	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ���������� ���:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(k)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		td.copy(k);
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	return 0;
}

int Shift_r(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int t;


	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ����� ������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(t)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		td.shift_right(t);
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}

	return 0;
}

int Shift_l(time_diag1& td)
{
	const char *pr = ""; //������� ��������� �� ������
	int fl;
	int t;
	time_diag1 tmp;

	pr = "";
	fl = 0;

	do{
		cout << pr << endl;
		if (fl == 2)
			cout << endl;
		cout << "������� ����� ������:  --> ";
		pr = "�� ��������. ��������� ����, ����������!";
		if ((fl = getNum(t)) < 0)
			return 0;
		if (fl == 1)
			return 1;
	} while (fl == 2);

	try{
		tmp = td.shift_left(t);
	}
	catch (const char *msg)
	{
		cerr << msg << endl;
	}
	td = tmp;

	return 0;
}

int Exit(time_diag1& td)
{
	return 1;
}