#pragma once

#include <SFML/Graphics.hpp>
#include "Level1.h"
#include <string>
#include <list>
#include <iostream>
#include <Windows.h>
#include <sstream>
using namespace sf;



class Entity1
{
public:
	float currentFrame, attacktimer, attacktimer2, timer, followtimer, stupidtimer;
	float x, y, w, h, dx, dy, speed;
	float px, py;
	bool life;
	String Name;
	Sprite sprite;
	int Health;
	int dir;

	Entity1();
		
	~Entity1(){};

	virtual void update(float time){};

	void getPlayerCoord(float X, float Y);

	FloatRect getRect()
	{
		return FloatRect(x, y, w, h);
	}
};
