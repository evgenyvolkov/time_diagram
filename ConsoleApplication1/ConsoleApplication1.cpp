#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <list>
#include <iostream>
#include <Windows.h>
#include <sstream>
#include "Bullet1.h"
#include "Enemy1.h"
#include "Entity1.h"
#include "PLAYER1.h"
#include "Level1.h"
#include "..\Laba2\time_diag1.h"
#define _WIN32_WINNT 0x0500 

double attacktime = 0;
using namespace sf;
using namespace std;
 
int main()
{
	RenderWindow window(VideoMode(1357, 690), "Smile tank!");
	window.setPosition(Vector2i(0, 0));
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(120);
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);

	Texture menu_texture1, menu_texture2, about_texture, mainmenu, menu_texture3, sovet_texture;
	menu_texture1.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\111.png");
	menu_texture2.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\333.png");
	menu_texture3.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\444.png");
	about_texture.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\about.jpg");
	sovet_texture.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\sovet.png");
	mainmenu.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\mainimg.png");
	mainmenu.setSmooth(true);
	menu_texture1.setSmooth(true);
	menu_texture2.setSmooth(true);
	menu_texture3.setSmooth(true);
	about_texture.setSmooth(true);
	sovet_texture.setSmooth(true);
	Sprite menu1(menu_texture1), menu2(menu_texture2), about(about_texture), main(mainmenu), menu3(menu_texture3), sovet(sovet_texture);
	bool Menu = 1;
	int MenuNum = 0;
	menu1.setPosition(100, 30);
	menu2.setPosition(100, 90);
	menu3.setPosition(280, 30);
	main.setScale(Vector2f(1357.0 / 1162.0, 690.0 / 646.0));
	sovet.setScale(Vector2f(1357.0 / 800.0, 690.0 / 500.0));
	while (Menu)
	{
		menu1.setColor(Color::Magenta);
		menu2.setColor(Color::Magenta);
		menu3.setColor(Color::Cyan);
		MenuNum = 0;
		window.draw(main);
		if (IntRect(100, 30, 169, 39).contains(Mouse::getPosition(window))) { menu1.setColor(Color::Red); MenuNum = 1; }
		if (IntRect(100, 90, 110, 41).contains(Mouse::getPosition(window))) { menu2.setColor(Color::Red); MenuNum = 2; }
		if (IntRect(280, 30, 110, 41).contains(Mouse::getPosition(window))) { menu3.setColor(Color::Red); MenuNum = 3; }
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed){
				window.close();
				Menu = false;
			}
		}

		if (Mouse::isButtonPressed(Mouse::Left))
		{
			if (MenuNum == 1) Menu = false;
			if (MenuNum == 2)  { window.close(); Menu = false; }
			if (MenuNum == 3) { window.draw(sovet); window.display(); while (!Keyboard::isKeyPressed(Keyboard::Escape)); }
		}

		window.draw(menu1);
		window.draw(menu2);
		window.draw(menu3);

		window.display();
	}
	////////////////

	Music music;
	music.openFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\music.ogg");
	music.play();

	SoundBuffer buffer;
	buffer.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\enemyboom.ogg");
	Sound sound(buffer);

	SoundBuffer popadanie;
	popadanie.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\popal.ogg");
	Sound playerpopal(popadanie);

	SoundBuffer epopal;
	epopal.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\enemypopal.ogg");
	Sound enemypopal(epopal);

	SoundBuffer gameover;
	gameover.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\gameover.ogg");
	Sound gameend(gameover);

	SoundBuffer shoot;
	shoot.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\shoot.ogg");
	Sound vistrel(shoot);


	sf::View view;
	view.reset(sf::FloatRect(0, 0, 1357, 690));

	Image playerimage;
	playerimage.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\heronewnewnewcopy.png");
	playerimage.createMaskFromColor(Color(0, 0, 0));
	Texture hero; // ������ �������� �������� �����
	hero.loadFromImage(playerimage);
	hero.setSmooth(true);
	PLAYER1 p(hero);

	Image enemyimage;
	enemyimage.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\enemytank.png");
	enemyimage.createMaskFromColor(Color(0, 0, 0));
	Texture enemyt;
	enemyt.loadFromImage(enemyimage);
	enemyt.setSmooth(true);

	Texture map;//�������� �����
	map.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\map.png");//��������� ���� ��� �����
	map.setSmooth(true);
	Sprite s_map;//������ ������ ��� �����
	s_map.setTexture(map);

	Image bullet;
	bullet.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\bullet.png");
	bullet.createMaskFromColor(Color(0, 0, 0));//������������ ���� ����
	Texture bul_texture;
	bul_texture.loadFromImage(bullet);
	bul_texture.setSmooth(true);

	float gametimer = 0;
	float timer = 0;
	float attacktimer = 0;
	float attacktimer2 = 0;
	float gameovertime = 0;

	std::list<Entity1*> entities;
	std::list<Entity1*>::iterator it;
	std::list<Entity1*>::iterator it2;

	entities.push_back(new Enemy1(enemyt, 2890, 370, "Enemy"));
	entities.push_back(new Enemy1(enemyt, 1000, 60, "Enemy"));
	entities.push_back(new Enemy1(enemyt, 2890, 600, "Enemy"));

	std::list<Enemy1*> enemys;
	std::list<Enemy1*>::iterator ite;

	float PlayerCurrentFrame = 0; // �������������� ����������. ������� ������� ��������. � ���� ���������� �������� ����� �������� �����

	Font font;
	font.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\sansation.ttf");
	Text mytext("", font, 48);

	mytext.setColor(Color::Black);
	String str;
	int count = 0;
	Clock clock; //������� ���������� �������, �������� �� �������(� �� ������ ����������). ����� �� ���� ����������� ���� �������� � ���������� ���������
	RectangleShape rectangle; //��������������� ���������� ��� ���������� �����
	int index = 0;
	while (window.isOpen() /*&& p.life*/&& count < 53 )
	{
		float time = clock.getElapsedTime().asMicroseconds(); //���� ��������� ����� � �������������
		clock.restart(); //������������� �����
	
		time = time / 800; //�������� ����
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
			if (event.type == Event::KeyPressed)
			if (event.key.code == Keyboard::Space) { if (p.life){ entities.push_back(new Bullet1(bul_texture, p.x, p.y, p.dir, "Bullet")); vistrel.play(); } }
		}
		if ((Keyboard::isKeyPressed(Keyboard::Left))) { if (p.life){ p.dir = 1; p.speed = 0.2; p.spritehero.setTextureRect(IntRect(0, 0, 112, 98)); } }
		if ((Keyboard::isKeyPressed(Keyboard::Right))) { if (p.life){ p.dir = 0; p.speed = 0.2; p.spritehero.setTextureRect(IntRect(0, 0, 112, 98)); } }
		if ((Keyboard::isKeyPressed(Keyboard::Up))) { if (p.life){ p.dir = 3; p.speed = 0.2; p.spritehero.setTextureRect(IntRect(0, 0, 112, 98)); } }
		if ((Keyboard::isKeyPressed(Keyboard::Down))) { if (p.life){ p.dir = 2; p.speed = 0.2; p.spritehero.setTextureRect(IntRect(0, 0, 112, 98)); } }
		if ((Keyboard::isKeyPressed(Keyboard::N)))  { 
			if (p.life){
				PlayerCurrentFrame += 0.015*time;// �������� �������� 0.005
				if (PlayerCurrentFrame > 5) PlayerCurrentFrame -= 5;
				p.spritehero.setTextureRect(IntRect(117 * int(PlayerCurrentFrame), 196, 115, 167));
				p.speed = 0.35;
			}
		}
		if ((Keyboard::isKeyPressed(Keyboard::Escape)))  { window.close(); }
		if ((Keyboard::isKeyPressed(Keyboard::I)))  { view.setSize(1357, 690); }
		if ((Keyboard::isKeyPressed(Keyboard::Subtract)))  { view.zoom(1.0100f); }
		if ((Keyboard::isKeyPressed(Keyboard::Add)))  { view.zoom(0.99f); }

		for (it = entities.begin(); it != entities.end();)
		{
			Entity1 *b = *it;
			if (b->life == false)	{
				if (b->Name == "Enemy") entities.push_back(new Enemy1(enemyt, 2890 - rand() % 1400, 370, "Enemy"));
				it = entities.erase(it);   delete b;
			}
			else   it++;
		}

			for (it = entities.begin(); it != entities.end(); it++)
			{
				(*it)->getPlayerCoord(p.x, p.y);
				if (p.life)
					(*it)->update(time);

				if ((*it)->Name == "Enemy")
				if (p.life)
				if ((*it)->attacktimer > 3500) {
					bool q = false;
					if (((*it)->x - p.x<55) && ((*it)->y>p.y))
					{
						(*it)->speed = 0.1, (*it)->dir = 3; q = 1;
					}
					if (((*it)->x - p.x < 54) && ((*it)->y < p.y)) { (*it)->speed = 0.2, (*it)->dir = 2; q = 1; }
					if ((p.y - (*it)->y < 53) && ((*it)->x<p.x)) { (*it)->speed = 0.2, (*it)->dir = 0; q = 1; }
					if (((*it)->y - p.y<56) && ((*it)->x>p.x)) { (*it)->speed = 0.2, (*it)->dir = 1; q = 1; }
					(*it)->attacktimer = 0;
					if (q) entities.push_back(new Bullet1(bul_texture, (*it)->x, (*it)->y, (*it)->dir, "enemyBullet"));
				}

				if ((*it)->Name == "Enemy")
				if (p.life)
				if ((*it)->getRect().intersects(p.getRect()))
				{
					if ((*it)->dy>0)
					{
						(*it)->y = p.y - p.h;
					}
					if ((*it)->dy<0)
					{
						(*it)->y = p.y + p.h;
					}
					if ((*it)->dx>0)
					{
						(*it)->x = p.x - p.w;
					}
					if ((*it)->dx < 0)
					{
						(*it)->x = p.x + p.w;
					}

				}

				if ((*it)->Name == "enemyBullet")

				if ((*it)->getRect().intersects(p.getRect()))
				{
					(*it)->life = 0;
					p.Health -= 50;//�������� �������� � ������ ������
					if (p.Health<100) p.spritehero.setColor(Color::Red);
					enemypopal.play();
				}

				if ((*it)->Name == "Enemy")
				for (it2 = entities.begin(); it2 != entities.end(); it2++)
				{
					Entity1 *mem = 0;
					if ((*it2)->Name == "Bullet"){
						if ((*it)->getRect().intersects((*it2)->getRect()))
						{
							(*it2)->life = 0;
							(*it)->Health -= 50;//������� ������, ������ �� ����,�����,������� �����
			
							(*it)->sprite.setColor(Color::Yellow);
							playerpopal.play();
							if ((*it)->Health <= 0 && (*it2) != mem) { sound.play(); time_diag1 ptr1('x', 50); mem = (*it2); points = points + ptr1; }
							else{
								time_diag1 ptr('1', 30);
								points = points + ptr;
							}

						}
					}


					if ((*it2)->Name == "Enemy")
					if (p.life)
					if ((*it)->getRect() != (*it2)->getRect())
					if ((*it)->getRect().intersects((*it2)->getRect()))
					{

						if (((*it)->dy) > 0)
						{
							(*it)->y = ((*it2)->y) - ((*it2)->h);
						}

						if ((*it)->dy<0)
						{
							(*it)->y = (*it2)->y + (*it2)->h;
						}
						if ((*it)->dx>0)
						{
							(*it)->x = (*it2)->x - (*it2)->w;
						}
						if ((*it)->dx < 0)
						{
							(*it)->x = (*it2)->x + (*it2)->w;
						}
					}
				}
			}
			int x0 = p.x;
			int y0 = p.y;
			if (p.x<1357 / 2) x0 = 1357 / 2;
			if (p.y<690 / 2) y0 = 690 / 2;
			if (p.x>32 * 73.75) x0 = 32 * 73.75;
			if (p.y>17.25 * 32) y0 = 17.25 * 32;
			
			if (p.life)
				view.setCenter(x0, y0);
			p.update(time);

			for (it = entities.begin(); it != entities.end(); it++)
			if ((*it)->Name == "Enemy")
			if (p.life)
			if ((*it)->getRect().intersects(p.getRect()))
			{
				if (p.dir == 2)
				{
					p.y = (*it)->y - (*it)->h;
				}
				if (p.dir == 3)
				{
					p.y = (*it)->y + (*it)->h;
				}
				if (p.dir == 0)
				{
					p.x = (*it)->x - (*it)->w;
				}
				if (p.dir == 1)
				{
					p.x = (*it)->x + (*it)->w;
				}

			}

			
			if (p.Health <= 0 && count < 1)//!!!!!!!!!!
			{
				music.stop();
				p.life = false;
				std::ostringstream ss;
				int bom = points.Get_b_d();
				p.score = p.score + bom;
				ss << p.score;
				str = ss.str();
				mytext.setString(str);
			}

			

			window.clear(Color(128, 106, 89));
			//������ �����
			for (int i = 0; i < H; i++)
			for (int j = 0; j < W; j++)
			{
				if (TileMap[i][j] == '0')  s_map.setTextureRect(IntRect(32, 0, 32, 32)); //0-����� ������������� 
				if (TileMap[i][j] == 'B')  s_map.setTextureRect(IntRect(64, 0, 32, 32));//B-����������� �����
				if ((TileMap[i][j] == ' ')) s_map.setTextureRect(IntRect(0, 0, 32, 32));//" "-������ ����
				if ((TileMap[i][j] == 'R') || (TileMap[i][j] == 'F') || (TileMap[i][j] == 'P')) s_map.setTextureRect(IntRect(96, 0, 32, 32));//R,F,P-��������
				if (TileMap[i][j] == 'E')  s_map.setTextureRect(IntRect(160, 0, 32, 32));//E-�������� � ��������
				if (TileMap[i][j] == 'S')  s_map.setTextureRect(IntRect(196, 0, 32, 32));//S-����� ����������� �����������
				if (TileMap[i][j] == 'G')  s_map.setTextureRect(IntRect(224, 0, 32, 32));//G-���������
				if (TileMap[i][j] == 'D')  s_map.setTextureRect(IntRect(256, 0, 32, 32));//D-�������� ��������
				if (TileMap[i][j] == 'Q')  s_map.setTextureRect(IntRect(320, 0, 32, 32));//Q-�������������� ����

				s_map.setPosition(j * 32, i * 32);
				window.draw(s_map);
			}

			window.setView(view);
			window.draw(p.spritehero);	//������� �������� �� �����
			if (p.life)
			for (it = entities.begin(); it != entities.end(); it++)
				window.draw((*it)->sprite); //������ ��������
			p.dx = p.dy = 0;
			if (!p.life){
				++count;
			}
			window.display();
		
	}
	while (window.isOpen()){
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		if ((Keyboard::isKeyPressed(Keyboard::Escape)))  { window.close(); }
		Texture gameover;
		gameover.loadFromFile("C:\\Users\\�������\\Documents\\Visual Studio 2013\\Projects\\Laba2\\Debug\\gameover.jpg");
		gameover.setSmooth(true);
		Sprite gameov(gameover);
		gameov.setScale(Vector2f(1357.0 / 900.0, 690.0 / 583.0));
		window.clear();
		view.reset(sf::FloatRect(0, 0, 1357, 690));
		window.setView(view);
		window.draw(gameov);

		Text mytext1("Game Over!", font, 80);
		mytext1.setColor(Color::Red);
		mytext1.setPosition(1357 / 2 - 10 * 21, 690 / 2 - 120);

		Text mytext2("Press #esc to exit. Score: ", font, 48);
		mytext2.setColor(Color::Red);
		mytext2.setPosition(1357 / 2 - 10 * 40, 690 / 2);

		mytext.setPosition(1357 / 2 + 160, 690 / 2);
		mytext.setColor(Color::Red);

		window.draw(mytext1);
		window.draw(mytext2);
		window.draw(mytext);

		window.display();
	}
	
	return 0;
}