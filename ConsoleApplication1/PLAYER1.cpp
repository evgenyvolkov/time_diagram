#include "stdafx.h"
#include "PLAYER1.h"
#include "Level1.h"


PLAYER1::PLAYER1(Texture &Hero)
{
	spritehero.setTexture(Hero); // ��������� ��������
	x = 300;
	y = 600;
	w = 112;
	h = 98; // ��������� �������������� ����������,������ � ������
	spritehero.setTextureRect(IntRect(0, 0, w, h));  //���������� �����
	dx = dy = 0;
	currentFrame = 0;
	dir = 0;
	life = true;
	score = 0;
	speed = 0.2;//��������� ��������
	spritehero.setOrigin(w / 2, h / 2);
	onSpeed = 0;
	first = spritehero.getColor();
}


void PLAYER1::update(float time)
{
	if (onSpeed) speed = 0.8;
	if (Health <= 0) speed = 0;
	switch (dir)
	{
	case 0: dx = speed; dy = 0; spritehero.setRotation(90);  break;
	case 1: dx = -speed; dy = 0; spritehero.setRotation(-90);  break;
	case 2: dx = 0; dy = speed; spritehero.setRotation(180);  break;
	case 3: dx = 0; dy = -speed;  spritehero.setRotation(0); break;
	}
	timer += time;
	x += dx*time;
	y += dy*time;

	for (int i = y / 32; i<(y + h) / 32; i++)
	for (int j = x / 32; j<(x + w) / 32; j++)
	{
		if ((TileMap[i][j] == 'B') || (TileMap[i][j] == '0') || (TileMap[i][j] == 'S'))
		{
			if (dy>0)
			{
				y = i * 32 - h;
			}
			if (dy<0)
			{
				y = i * 32 + 32;
			}
			if (dx>0)
			{
				x = j * 32 - w;
			}
			if (dx<0)
			{
				x = j * 32 + 32;
			}
		}
		if (TileMap[i][j] == 'P' && life) { x = 300; y = 350; speed = 0; }
		if (TileMap[i][j] == 'R' && life) { x = 600; y = 200; speed = 0; }
		if (TileMap[i][j] == 'F' && life) { x = 2400; y = 400; speed = 0; }
		if (TileMap[i][j] == 'Q' && life) { TileMap[i][j] = ' '; time_diag1 pp('0', 20); points = points + pp; }
		if (TileMap[i][j] == 'D' && life) { TileMap[i][j] = ' '; Health -= 30; spritehero.setColor(Color::Yellow); }
		if (TileMap[i][j] == 'E' && life) {
			TileMap[i][j] = ' ';
			if (Health >= 100){
				Health += 50;
				spritehero.setColor(Color::Green);
			}
			if (Health < 100){
				Health = 100;
				spritehero.setColor(first);
			}
		}
		if (TileMap[i][j] == 'G' && life) { onSpeed = true; dir = 0; }
	}
	spritehero.setPosition(x + w / 2, y + h / 2); //������� ������ � ������� x y 

	if (onSpeed) stupidtimer += time;
	if (stupidtimer>1500) { onSpeed = 0; stupidtimer = 0; }
	speed = 0;

	if (Health <= 0) {

		currentFrame += 0.007*time;// �������� �������� 0.007
		if (currentFrame > 8) currentFrame -= 8;
		spritehero.setTextureRect(IntRect(85 * int(currentFrame), 99, 82, 86));
		spritehero.setColor(first);
	}
}
