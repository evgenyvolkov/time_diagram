**What is this repository for?**

This repository contains the solution for the task "Time diagram", variant 12. It describes the time diagram with some operations (creating, input, union, replace, copying, shift etc.) and GUI(graphic application).

**How do I get set up?**

This repository contains solution, created in VS2013 using C++. To open it you should have VS2013 installed and run Laba2.sln. If you want to run tests, you should have GoogleTest library included and Set gtest2_1 or gtest2_2 as StartUp project in properties. If you want to run dialog program, you should Set as StartUp project ConsoleApplication2_1 or ConsoleApplication2_2 in properties. If you want to run graphic application, you should Set as StartUp project ConsoleApplication1 in properties

**Who do I talk to?**

Volkov Evgeny, group K03-121, NRNU MEPhI.