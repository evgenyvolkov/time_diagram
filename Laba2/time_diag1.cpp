//-------------------------------------------
//time_diag1.cpp
//����������� ������� ������ ��������� ���������
//
//-------------------------------------------


#include "stdafx.h"
#include "time_diag1.h"
#include <stdexcept>
#include <stdio.h>
#include <string.h>

#define _USE_MATH_DEFINES	//���������� ��� ����������� �������������� ��������, � ��������� PI
#include <cmath>			//���������� ��� ����������� ��� ��������� ���. ��������


time_diag1::time_diag1()
{
	busy_val = 0;
	busy_dur = 0;
	mas = nullptr;
}

time_diag1::~time_diag1()
{
	delete[] mas;
}

time_diag1::time_diag1(char val_, int dur_)
{
	mas = nullptr;
	if (dur_ > max_dur)
		throw ("Too big duration!!!");
	if (dur_ <= 0)
		throw ("Incorrect data!!!");
	if (val_ != '0' && val_ != '1')
		val_ = 'x';
	try{
		mas = new signal[1];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}
	mas[0].dur = dur_;
	mas[0].val = val_;
	busy_val = 1;
	busy_dur = dur_;
}

time_diag1::time_diag1(const time_diag1 &td)
{
	mas = nullptr;
	if (td.busy_val){
		try{
			mas = new signal[td.busy_val];
		}
		catch (bad_alloc)
		{
			throw ("allocate failure!!!");
		}
		for (int i = 0; i < td.busy_val; ++i){
			mas[i].dur = td.mas[i].dur;
			mas[i].val = td.mas[i].val;
		}
		busy_dur = td.busy_dur;
		busy_val = td.busy_val;
	}
	else{
		busy_val = 0;
		busy_dur = 0;
		mas = nullptr;
	}
}

time_diag1::time_diag1(const char* ascii)
{
	mas = nullptr;
	char c = ascii[0];
	char c1 = '\0';
	busy_dur = 0;
	busy_val = 0;
	time_diag1 ptr;
	int k;
	if (c == '\0')
		throw ("Null string!!!");

	int i = 0;
	int j = 0;

	try{
		ptr.mas = new signal[strlen(ascii)];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}

	while (c != '\0'){
		if (c != '0' && c != '1')
			c = 'x';

		c1 = ascii[i + 1];

		if (c1 != '0' && c1 != '1' && c1 != '\0')
			c1 = 'x';
		if (c == c1){
			k = 0;
			while (c != '\0' && c1 != '\0' && c == c1){
				c1 = ascii[i];
				if (c1 != '0' && c1 != '1' && c1 != '\0')
					c1 = 'x';
				++i;
				++k;
			}
			if (ptr.max_dur - ptr.busy_dur < k - 1)
			{
				
				throw ("Overflow time diagram!!!");

			}

			switch (c)
			{
			case '0':
			{
						ptr.mas[j].val = '0';
						break;
			}
			case '1':
			{
						ptr.mas[j].val = '1';
						break;
			}
			default:
			{
					   ptr.mas[j].val = 'x';

			}
			}
			ptr.mas[j].dur = k - 1;
			++ptr.busy_val;
			ptr.busy_dur += k - 1;
			i = i - 1;
			c = ascii[i];
			++j;
		}
		else{
			if (ptr.max_dur - ptr.busy_dur == 0){
				
				throw ("Overflow time diagram!!!");
			}
			switch (c)
			{
			case '0':
			{
						ptr.mas[j].val = '0';
						break;
			}
			case '1':
			{
						ptr.mas[j].val = '1';
						break;
			}
			default:
			{
					   ptr.mas[j].val = 'x';

			}
			}
			ptr.mas[j].dur = 1;
			++ptr.busy_val;
			++ptr.busy_dur;
			++i;
			++j;
			c = ascii[i];
		}
	}
	try{
		mas = new signal[ptr.busy_val];
	}
	catch (bad_alloc)
	{
		
		throw ("allocate failure!!!");
	}
	for (i = 0; i < ptr.busy_val; ++i){
		mas[i].dur = ptr.mas[i].dur;
		mas[i].val = ptr.mas[i].val;
	}
	busy_dur = ptr.busy_dur;
	busy_val = ptr.busy_val;
}

time_diag1& time_diag1:: operator = (const time_diag1& td)
{
	if (this != &td){
		signal *tmas;
		if (td.busy_val != 0){
			try{
				tmas = new signal[td.busy_val];
			}
			catch (bad_alloc &ba)
			{
				throw ("allocate failure!!!");
			}
			delete[] mas;
			mas = tmas;
			for (int i = 0; i < td.busy_val; ++i){
				mas[i].dur = td.mas[i].dur;
				mas[i].val = td.mas[i].val;
			}
			busy_dur = td.busy_dur;
			busy_val = td.busy_val;
		}
		else{
			delete[] mas;
			busy_val = 0;
			busy_dur = 0;
			mas = nullptr;
		}
	}
	return *this;
}

time_diag1& time_diag1:: operator = (time_diag1 && td)
{
	int tbusy_val = busy_val;
	int tbusy_dur = busy_dur;
	busy_val = td.busy_val;
	busy_dur = td.busy_dur;
	td.busy_val = tbusy_val;
	td.busy_dur = tbusy_dur;
	signal *tmas = mas;

	mas = td.mas;
	td.mas = tmas;

	return *this;
}

void time_diag1::Set(char s_val, int s_dur, int ind)
{
	if (max_dur - busy_dur < s_dur)
		throw ("Too big duration!!!");
	if (s_dur <= 0 || ind < 0 || ind > busy_val)
		throw ("Incorrect data!!!");
	if (s_val != '0' && s_val != '1')
		s_val = 'x';
	if (ind  > busy_val - 1){
		if (ind > 0 && s_val == mas[ind - 1].val){
			mas[ind - 1].dur += s_dur;
			busy_dur += s_dur;
		}
		else{
			signal *tmas=nullptr;
			try{
				tmas = new signal[busy_val+1];
			}
			catch (bad_alloc)
			{
				throw ("allocate failure!!!");
			}
			for (int i = 0; i < busy_val; ++i){
				tmas[i].dur = mas[i].dur;
				tmas[i].val = mas[i].val;
			}
			delete[] mas;
			tmas[ind].val = s_val;
			tmas[ind].dur = s_dur;
			mas = tmas;
			tmas = nullptr;
			++busy_val;
			busy_dur += s_dur;
		}
	}
	else
	{
		busy_dur = busy_dur - mas[ind].dur + s_dur;
		mas[ind].val = s_val;
		mas[ind].dur = s_dur;
		correct();
	}
}

char time_diag1::Get_val(int ind)const
{
	if (ind < 0  || ind > busy_val - 1)
		throw ("Incorrect data!!!");


	return mas[ind].val;
};

int time_diag1::Get_dur(int ind)const
{
	if (ind < 0  || ind > busy_val - 1)
		throw ("Incorrect data!!!");


	return mas[ind].dur;
};

istream & operator >> (istream & s, time_diag1 & td)
{

	time_diag1 ptr;
	int k=20;
	//���������� ��� �������� ����� ���������
	try{
		ptr.mas = new time_diag1::signal[k];
	}
	catch (bad_alloc)
	{
		s.setstate(std::ios::failbit);
		return s;
	}
	int kol;
	s >> kol;
	for (int i = 0; i < kol ; ++i){
		if (ptr.busy_val == k){
			k += k;
			time_diag1::signal *tmas;
			try{
				tmas = new time_diag1::signal[k];
			}
			catch (bad_alloc)
			{
				s.setstate(std::ios::failbit);
				return s;
			}
			for (int j = 0; j < ptr.busy_val; ++j){
				tmas[j].dur = ptr.mas[j].dur;
				tmas[j].val = ptr.mas[j].val;
			}
			delete[] ptr.mas;
			ptr.mas = tmas;
			tmas = nullptr;
		}
		s >> ptr.mas[i].val;
		if (s.eof())
			return s;;
		if (ptr.mas[i].val != '0' && ptr.mas[i].val != '1')
			ptr.mas[i].val = 'x';
		s >> ptr.mas[i].dur;
		if (s.eof() || !s.good())
			return s;
		if (ptr.mas[i].dur <= 0 || ptr.max_dur - ptr.busy_dur < ptr.mas[i].dur){
			s.setstate(std::ios::failbit);
			return s;
		}
		++ptr.busy_val;
		ptr.busy_dur += ptr.mas[i].dur;
	}
	try{
		td.mas = new time_diag1::signal[ptr.busy_val];
	}
	catch (bad_alloc)
	{
		
		s.setstate(std::ios::failbit);
		return s;
	}
	for (int i = 0; i < ptr.busy_val; ++i){
		td.mas[i].dur = ptr.mas[i].dur;
		td.mas[i].val = ptr.mas[i].val;
	}

	td.busy_dur = ptr.busy_dur;
	td.busy_val = ptr.busy_val;
	td.correct();
	return s;
}

ostream & operator << (ostream &c, const time_diag1 & td)
{
	for (int i = 0; i < td.busy_val; ++i)
		c << '<' << td.mas[i].val << ',' << td.mas[i].dur << '>' << ' ';
	return c;
}

void time_diag1::correct()
{
	int k, i, i1 = -1, m = 0, s = 0;
	char c;
	signal *tmas;
	while (i1 + 1 < busy_val){
		s = 0;
		for (i = 0; i < busy_val - 1; ++i)
		if (mas[i].val == mas[i + 1].val){
			c = mas[i].val;
			++s;
			break;
		}

		if (s){

			i1 = i;
			k = 0;
			int f = 0;
			while (mas[i].val == c){
				k += mas[i].dur;
				++i;
				++f;
			}
			--f;
			mas[i1].dur = k;
			for (int j = i1 + 1; i < busy_val; ++j, ++i){
				mas[j].val = mas[i].val;
				mas[j].dur = mas[i].dur;
			}
			busy_val = busy_val - f;
			try{
				tmas = new signal[busy_val];
			}
			catch (bad_alloc)
			{
				throw ("allocate failure!!!");
			}
			for (i = 0; i < busy_val; ++i){
				tmas[i].val = mas[i].val;
				tmas[i].dur = mas[i].dur;
			}
			delete[] mas;
			mas = tmas;
		}
		else
			break;
	}
}

 time_diag1 operator + (const time_diag1 &td, const time_diag1 &td1)
{
	time_diag1 tmp;

	if (td1.busy_dur + td.busy_dur > tmp.max_dur)
		throw ("Overflow time diagram!!!");
	try{
		tmp.mas = new time_diag1::signal[td1.busy_val + td.busy_val];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}
	
	for (int i = 0; i < td.busy_val; ++i){
		tmp.mas[i].val = td.mas[i].val;
		tmp.mas[i].dur = td.mas[i].dur;
	}
	int j = td.busy_val;
	for (int i = 0; i < td1.busy_val; ++i, ++j){
		tmp.mas[j].val = td1.mas[i].val;
		tmp.mas[j].dur = td1.mas[i].dur;
	}
	tmp.busy_dur = td1.busy_dur + td.busy_dur;
	tmp.busy_val = td1.busy_val + td.busy_val;

	tmp.correct();

	return tmp;
}

time_diag1& time_diag1:: operator () (const time_diag1 &td, int time)
{
	int t = 0;
	int i = -1;

	if (time < 0 || time > busy_dur)
		throw ("Incorrect data!!!");
	if (max_dur < (busy_dur - time) + td.busy_dur)
		throw ("Overflow time diagram!!!");

	while (t < time){
		++i;
		t += mas[i].dur;
	}
	int f = t - time;
	if (i == -1){
		i = 0;
		f = mas[0].dur;
	}

	int i1 = i + 1;


	int t1 = 0;
	int g = 0;
	int j = i;
	int ind = 0;
	if (td.mas[0].dur > f){
		++j;
		t1 += td.mas[0].dur;
	}
	while (t1 < td.busy_dur){
		t1 += td.mas[ind].dur;
		ind++;
		if (t1 > mas[j].dur){
			g += mas[j].dur;
			++j;
		}
	}
	int kol = 0;
	time_diag1 start;
	time_diag1 tmp;
	time_diag1 finish;

	try{
		start.mas = new signal[i + 1];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}

	if (i == j){
		if (f != mas[i].dur && f > 0){
			
			for (int m = 0, k = 0; k < i; ++m, ++k){
				start.mas[m].val = mas[k].val;
				start.mas[m].dur = mas[k].dur;
				++start.busy_val;
				start.busy_dur += mas[k].dur;
			}
			start.mas[start.busy_val].val = mas[i].val;
			start.mas[start.busy_val].dur = mas[i].dur - f;
			++start.busy_val;
			start.busy_dur += mas[i].dur - f;
		}
		if (f != mas[i].dur && f == 0){
			
			for (int m = 0, k = 0; k <= i; ++m, ++k){
				start.mas[m].val = mas[k].val;
				start.mas[m].dur = mas[k].dur;
				++start.busy_val;
				start.busy_dur += mas[k].dur;
			}
		}
		if (f - td.busy_dur > 0){
			try{
				finish.mas = new signal[busy_val-i];
			}
			catch (bad_alloc)
			{
				
				throw ("allocate failure!!!");
			}
			finish.mas[0].val = mas[i].val;
			finish.mas[0].dur = f - td.busy_dur;
			++finish.busy_val;
			finish.busy_dur += f - td.busy_dur;
			for (int m = 1, k = i + 1; k < busy_val; ++m, ++k){
				finish.mas[m].val = mas[k].val;
				finish.mas[m].dur = mas[k].dur;
				++finish.busy_val;
				finish.busy_dur += mas[k].dur;
			}
		}
		else{
			try{
				finish.mas = new signal[busy_val-i-1];
			}
			catch (bad_alloc)
			{
			
				throw ("allocate failure!!!");
			}
			for (int m = 1, k = i + 1; k < busy_val; ++m, ++k){
				finish.mas[m].val = mas[k].val;
				finish.mas[m].dur = mas[k].dur;
				++finish.busy_val;
				finish.busy_dur += mas[k].dur;
			}
		}

	}
	else{
		if (f != mas[i].dur && f > 0){
			
			for (int m = 0, k = 0; k < i; ++m, ++k){
				start.mas[m].val = mas[k].val;
				start.mas[m].dur = mas[k].dur;
				++start.busy_val;
				start.busy_dur += mas[k].dur;
			}
			start.mas[start.busy_val].val = mas[i].val;
			start.mas[start.busy_val].dur = mas[i].dur - f;
			++start.busy_val;
			start.busy_dur += mas[i].dur - f;
		}
		if (f != mas[i].dur && f == 0){
			
			for (int m = 0, k = 0; k <= i; ++m, ++k){
				start.mas[m].val = mas[k].val;
				start.mas[m].dur = mas[k].dur;
				++start.busy_val;
				start.busy_dur += mas[k].dur;
			}
		}
		if (j <= busy_val - 1){
			if (g - td.busy_dur > 0){
				try{
					finish.mas = new signal[busy_val-j];
				}
				catch (bad_alloc)
				{
					
					throw ("allocate failure!!!");
				}
				finish.mas[0].val = mas[j].val;
				finish.mas[0].dur = g - td.busy_dur;
				++finish.busy_val;
				finish.busy_dur += g - td.busy_dur;
				for (int m = 1, k = j + 1; k < busy_val; ++m, ++k){
					finish.mas[m].val = mas[k].val;
					finish.mas[m].dur = mas[k].dur;
					++finish.busy_val;
					finish.busy_dur += mas[k].dur;
				}
			}
			else{
				try{
					finish.mas = new signal[busy_val - j - 1];
				}
				catch (bad_alloc) 
				{
					
					throw ("allocate failure!!!");
				}
				for (int m = 0, k = j + 1; k < busy_val; ++m, ++k){
					finish.mas[m].val = mas[k].val;
					finish.mas[m].dur = mas[k].dur;
					++finish.busy_val;
					finish.busy_dur += mas[k].dur;
				}
			}
		}
		
	}
	tmp = tmp + start;

	tmp = tmp + td;

	tmp = tmp + finish;

	signal *tmas;

	try{
		tmas = new signal[tmp.busy_val];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}

	for (int m = 0, k = 0; k < tmp.busy_val; ++m, ++k){
		tmas[m].val = tmp.mas[k].val;
		tmas[m].dur = tmp.mas[k].dur;
	}
	delete[] mas;
	mas = tmas;
	tmas = nullptr;
	busy_dur = tmp.busy_dur;
	busy_val = tmp.busy_val;

	return *this;
}

time_diag1& time_diag1::copy(int count)
{
	if (count < 1)
		throw ("Incorrect data!!!");
	if (count*busy_dur + busy_dur > max_dur)
		throw ("Overflow time diagram!!!");

	int k = 0;
	int f = busy_val;
	int g = busy_dur;
	signal *tmas;

	try{
		tmas = new signal[count*busy_val + busy_val];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}

	for (int j = 0; j < busy_val; ++j){
		tmas[j].val = mas[j].val;
		tmas[j].dur = mas[j].dur;
	}
	delete[] mas;

	while (k < count){
		for (int i = busy_val, j = 0; i < 2 * busy_val, j < f; ++i, ++j){
			tmas[i].dur = tmas[j].dur;
			tmas[i].val = tmas[j].val;
		}
		busy_dur += g;
		busy_val += f;
		++k;
	}
	mas = tmas;
	tmas = nullptr;

	correct();

	return *this;
}

time_diag1& time_diag1::shift_right(int time)
{
	int  i;
	if (time < 0)
		throw ("Incorrect data!!!");
	if (time + busy_dur > max_dur)
		throw ("Overflow time diagram!!!");
	signal *tmas;
	try{
		tmas = new signal[busy_val + 1];
	}
	catch (bad_alloc)
	{
		throw ("allocate failure!!!");
	}
	for (int k = 0; k < busy_val; ++k){
		tmas[k].val = mas[k].val;
		tmas[k].dur = mas[k].dur;
	}
	delete[] mas;
	for (i = busy_val; i > 0; --i){
		tmas[i].dur = tmas[i - 1].dur;
		tmas[i].val = tmas[i - 1].val;
	}
	tmas[0].dur = time;
	tmas[0].val = 'x';
	++busy_val;
	busy_dur += time;
	mas = tmas;
	tmas = nullptr;

	correct();

	return *this;
}

const time_diag1 time_diag1::shift_left(int time)const
{
	int  i = -1, t = 0;
	time_diag1 tmp;

	if (time < 0 || time > max_dur)
		throw ("Incorrect data!!!");


	while (t < time){
		++i;
		t += mas[i].dur;
	}
	
	if (t > time){
		try{
			tmp.mas = new signal[busy_val - i];
		}
		catch (bad_alloc)
		{
			throw ("allocate failure!!!");
		}
		tmp.mas[0].dur = mas[i].dur - (t - time);
		tmp.mas[0].val = mas[i].val;
		for (int j = 1, i1 = i + 1; j < busy_val - i, i1 < busy_val; ++j, ++i1){
			tmp.mas[j].dur = mas[i1].dur;
			tmp.mas[j].val = mas[i1].val;
		}
		tmp.busy_val = busy_val - i;
		tmp.busy_dur = busy_dur - time;
	}
	else{
		try{
			tmp.mas = new signal[busy_val - i - 1];
		}
		catch (bad_alloc)
		{
			throw ("allocate failure!!!");
		}
		for (int j = 0, i1 = i + 1; j < busy_val - i - 1, i1 < busy_val; ++j, ++i1){
			tmp.mas[j].dur = mas[i1].dur;
			tmp.mas[j].val = mas[i1].val;
		}
		tmp.busy_val = busy_val - i - 1;
		tmp.busy_dur = busy_dur - time;
	}

	return tmp;
}

