#pragma once
#include <iostream>
using namespace std;

class time_diag
{
private:
	static const int SZ=40;  //������ �������
	static const int max_dur=10000; //������������ ������������ ���������
	struct signal{
		char val; //�������� �������
		int dur; //������������ �������
	};
	signal mas[SZ];//������ �����������
	int busy_val;//���������� ������� �����
	int busy_dur;//���������� ������� ������������
	


public:
	time_diag();//������ ����������� ��� ������������� ����������� ������ �� ���������	
	~time_diag();
	time_diag(char val, int dur = max_dur); //������������� ���������� ������� ������� �� ������������ ������������
	time_diag(const char*); //������������� ������� ASCII ��������
	void Set(char s_val, int s_dur, int ind);	// ��������� ���������� �������
	char Get_val(int ind)const;  //����� �������� �������
	int Get_dur(int ind)const;  //����� ������������ �������
	int Get_b_v()const{ return busy_val; };
	int Get_b_d()const{ return busy_dur; };
	int Get_max_s()const{ return SZ; };
	int Get_max_d()const{ return max_dur; };
	friend istream & operator >> (istream & s, time_diag & td); //���� ����������� ������ �� �������� ������
	friend ostream & operator << (ostream &c, const time_diag & td); //����� ������������������ ������� � �������� �����
	friend const time_diag operator + (const time_diag &td, const time_diag &td1); //����������� ���� ��������� �������� <-friend
	time_diag& operator () (const time_diag &td, int time); //������ ��������� ��������� � ������������ ������ ������� ������ ��.����������
	time_diag& copy (int count); //����������� ��������� ��������� ������������ ����� ��� ������� �������
	time_diag& shift_right (int time); //����� ��������� ��������� �� ������������ ����� ������
	const time_diag shift_left (int time) const; //����� ��������� ��������� �� ������������ ����� �����
	void correct(); //������������� �������
};

