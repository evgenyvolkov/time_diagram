#pragma once
#include <iostream>
using namespace std;

class time_diag1
{
private:
	static const int max_dur = 10000; //������������ ������������ ���������
	struct signal{
		char val; //�������� �������
		int dur; //������������ �������
	};
	signal* mas;//������ �����������
	int busy_val;//���������� ������� �����
	int busy_dur;//���������� ������� ������������


	
public:
	time_diag1();//������ ����������� ��� ������������� ����������� ������ �� ���������	
	~time_diag1();
	time_diag1(const time_diag1 & td);//���������� �����������
	time_diag1(char val, int dur = max_dur); //������������� ���������� ������� ������� �� ������������ ������������
	time_diag1(const char*); //������������� ������� ASCII ��������
	time_diag1& operator = (const time_diag1& td); //��������������� ��������� ������
	time_diag1 (time_diag1 && td) : busy_val(td.busy_val), busy_dur(td.busy_dur), mas(td.mas)//������������ �����������
	{
		td.mas = nullptr;
	} 
	time_diag1& operator = (time_diag1 && td); //������������ �������� ������������
	void Set(char s_val, int s_dur, int ind);	// ��������� ���������� �������
	char Get_val(int ind)const;  //����� �������� �������
	int Get_dur(int ind)const;  //����� ������������ �������
	int Get_b_v()const{ return busy_val; };
	signal* Get_mas()const{ return mas; };
	int Get_b_d()const{ return busy_dur; };
	int Get_max_d()const{ return max_dur; };
	friend istream & operator >> (istream & s, time_diag1 & td); //���� ����������� ������ �� �������� ������
	friend ostream & operator << (ostream &c, const time_diag1 & td); //����� ������������������ ������� � �������� �����
	friend  time_diag1 operator + (const time_diag1 &td, const time_diag1 &td1); //����������� ���� ��������� �������� <-friend
	time_diag1& operator () (const time_diag1 &td, int time); //������ ��������� ��������� � ������������ ������ ������� ������ ��.����������
	time_diag1& copy(int count); //����������� ��������� ��������� ������������ ����� ��� ������� �������
	time_diag1& shift_right(int time); //����� ��������� ��������� �� ������������ ����� ������
	const time_diag1 shift_left(int time) const; //����� ��������� ��������� �� ������������ ����� �����
	void correct(); //������������� �������
};


