// gtest2_1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "..\Laba2\time_diag.h"
#include "gtest\gtest.h"
#include <cmath>



TEST(TestClassDiag, TestConstruct2)
{
	time_diag testD('0', 60);

	EXPECT_EQ(60, testD.Get_b_d());
	EXPECT_EQ(60, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
}

TEST(TestClassDiag, TestConstruct2_1)
{
	EXPECT_ANY_THROW(time_diag testD(0, 70));
}

TEST(TestClassDiag, TestConstruct2_2)
{
	EXPECT_ANY_THROW(time_diag testD(0, -5));
}

TEST(TestClassDiag, TestConstruct1)
{
	time_diag testD;

	EXPECT_EQ(0, testD.Get_b_d());
	EXPECT_EQ(0, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
}

TEST(TestClassDiag, TestConstruct3)
{
	char str[18] = "01xrt6r11001010x1";
	time_diag testD(str);

	EXPECT_EQ(17, testD.Get_b_d());
	EXPECT_EQ(11, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(5, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(2, testD.Get_dur(4));
	EXPECT_EQ(1, testD.Get_dur(5));
	EXPECT_EQ(1, testD.Get_dur(6));
	EXPECT_EQ(1, testD.Get_dur(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ(1, testD.Get_dur(9));
	EXPECT_EQ(1, testD.Get_dur(10));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	EXPECT_EQ('1', testD.Get_val(5));
	EXPECT_EQ('0', testD.Get_val(6));
	EXPECT_EQ('1', testD.Get_val(7));
	EXPECT_EQ('0', testD.Get_val(8));
	EXPECT_EQ('x', testD.Get_val(9));
	EXPECT_EQ('1', testD.Get_val(10));
	
}

TEST(TestClassDiag, TestConstruct3_1)
{
	char str[18] = "\0";
	EXPECT_ANY_THROW(time_diag testD(str));
}

TEST(TestClassDiag, TestConstruct3_2)
{
	char str[62] = "qwertyuioplkjhgfdsazxcvbnmlkjhgfdsaqwertyuioplkjhgfdsazxcvbnm";
	EXPECT_ANY_THROW(time_diag testD(str));
}

TEST(TestClassDiag, TestConstruct3_3)
{
	char str[45] = "10101010101010101010101010101010101010101010";
	EXPECT_ANY_THROW(time_diag testD(str));
}

TEST(TestClassDiag, TestConstructSet)
{
	time_diag testD;

	testD.Set('1', 4, 0);

	EXPECT_EQ(4, testD.Get_b_d());
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
	EXPECT_EQ(4, testD.Get_dur(0));
	EXPECT_EQ('1', testD.Get_val(0));
}

TEST(TestClassDiag, TestConstructSet_1)
{
	time_diag testD;

	EXPECT_ANY_THROW(testD.Set('1', 65, 0));
	EXPECT_ANY_THROW(testD.Set('1', -1, 0));
	EXPECT_ANY_THROW(testD.Set('1', 5, -3));
	EXPECT_ANY_THROW(testD.Set('1', 5, 2));
	EXPECT_ANY_THROW(testD.Set('1', 65, 42));
}

TEST(TestClassDiag, TestCorrect)
{
	time_diag testD;

	testD.Set('0', 2, 0);
	testD.Set('1', 3, 1);
	testD.Set('1', 2, 2);
	testD.Set('x', 2, 2);
	testD.Set('x', 4, 3);

	EXPECT_EQ(13, testD.Get_b_d());
	EXPECT_EQ(3, testD.Get_b_v());
	EXPECT_EQ(2, testD.Get_dur(0));
	EXPECT_EQ(5, testD.Get_dur(1));
	EXPECT_EQ(6, testD.Get_dur(2));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));


}

TEST(TestClassDiag, TestUnion)
{
	char str1[8] = "abcdefg";
	char str2[9] = "klmnoprs";
	char str3[8] = "1011011";
	char str4[9] = "10110100";
	time_diag testD1(str1);
	time_diag testD2(str2);
	time_diag testD3(str3);
	time_diag testD4(str4);
	time_diag testD;
	time_diag testD_;

	testD = testD1 + testD2;
	testD_ = testD3 + testD4;

	EXPECT_EQ(15, testD.Get_b_d());
	EXPECT_EQ(1, testD.Get_b_v());
	EXPECT_EQ(15, testD.Get_dur(0));
	EXPECT_EQ('x', testD.Get_val(0));

	EXPECT_EQ(15, testD_.Get_b_d());
	EXPECT_EQ(10, testD_.Get_b_v());
	EXPECT_EQ(1, testD_.Get_dur(0));
	EXPECT_EQ(1, testD_.Get_dur(1));
	EXPECT_EQ(2, testD_.Get_dur(2));
	EXPECT_EQ(1, testD_.Get_dur(3));
	EXPECT_EQ(3, testD_.Get_dur(4));
	EXPECT_EQ(1, testD_.Get_dur(5));
	EXPECT_EQ(2, testD_.Get_dur(6));
	EXPECT_EQ(1, testD_.Get_dur(7));
	EXPECT_EQ(1, testD_.Get_dur(8));
	EXPECT_EQ(2, testD_.Get_dur(9));
	EXPECT_EQ('1', testD_.Get_val(0));
	EXPECT_EQ('0', testD_.Get_val(1));
	EXPECT_EQ('1', testD_.Get_val(2));
	EXPECT_EQ('0', testD_.Get_val(3));
	EXPECT_EQ('1', testD_.Get_val(4));
	EXPECT_EQ('0', testD_.Get_val(5));
	EXPECT_EQ('1', testD_.Get_val(6));
	EXPECT_EQ('0', testD_.Get_val(7));
	EXPECT_EQ('1', testD_.Get_val(8));
	EXPECT_EQ('0', testD_.Get_val(9));
	
}

TEST(TestClassDiag, TestUnion_1)
{
	char str1[35] = "abcdefgqawesrdcfvghbzsxdcfvgbsexrd";
	char str2[37] = "klmnoprsxdcfvghbjjhgfcghkjhghjgchhgt";
	char str3[30] = "10101010101010101010101010101";
	char str4[20] = "1010101010101010101";
	time_diag testD1(str1);
	time_diag testD2(str2);
	time_diag testD;
	time_diag testD3(str3);
	time_diag testD4(str4);
	time_diag testD_;

	EXPECT_ANY_THROW(testD = testD1 + testD2);
	EXPECT_ANY_THROW(testD_ = testD3 + testD4);
}

TEST(TestClassDiag, TestReplace)
{
	char str1[8] = "abcdefg";
	char str2[4] = "klm";
	char str3[9] = "11111000";
	char str4[4] = "110";
	time_diag testD1(str1);
	time_diag testD2(str2);
	time_diag testD3(str3);
	time_diag testD4(str4);
	

	testD1(testD2, 3);
	testD3(testD4, 0);

	EXPECT_EQ(7, testD1.Get_b_d());
	EXPECT_EQ(1, testD1.Get_b_v());
	EXPECT_EQ(7, testD1.Get_dur(0));
	EXPECT_EQ('x', testD1.Get_val(0));


	EXPECT_EQ(8, testD3.Get_b_d());
	EXPECT_EQ(4, testD3.Get_b_v());
	EXPECT_EQ(2, testD3.Get_dur(0));
	EXPECT_EQ(1, testD3.Get_dur(1));
	EXPECT_EQ(2, testD3.Get_dur(2));
	EXPECT_EQ(3, testD3.Get_dur(3));
	EXPECT_EQ('1', testD3.Get_val(0));
	EXPECT_EQ('0', testD3.Get_val(1));
	EXPECT_EQ('1', testD3.Get_val(2));
	EXPECT_EQ('0', testD3.Get_val(3));
	
	
}//!!!!

TEST(TestClassDiag, TestReplace_1)
{
	char str1[30] = "10101010101010101010101010101";
	char str2[20] = "1010101010101010101";
	char str3[45] = "111110004g5s46g84es4g8es4g8e64g5444411111115";
	char str4[25] = "11010110fsd1fdsf1010v1se";
	time_diag testD1(str1);
	time_diag testD2(str2);
	time_diag testD3(str3);
	time_diag testD4(str4);
	

	EXPECT_ANY_THROW(testD1(testD2, -3));
	EXPECT_ANY_THROW(testD1(testD2, 31));
	EXPECT_ANY_THROW(testD3(testD4, 40));
	EXPECT_ANY_THROW(testD1(testD2, 25));
}

TEST(TestClassDiag, TestCopy)
{
	char str[12] = "10110100110";
	time_diag testD(str);

	testD.copy(3);

	EXPECT_EQ(44, testD.Get_b_d());
	EXPECT_EQ(32, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(2, testD.Get_dur(2));
	EXPECT_EQ(1, testD.Get_dur(3));
	EXPECT_EQ(1, testD.Get_dur(4));
	EXPECT_EQ(2, testD.Get_dur(5));
	EXPECT_EQ(2, testD.Get_dur(6));
	EXPECT_EQ(1, testD.Get_dur(7));
	EXPECT_EQ('1', testD.Get_val(0));
	EXPECT_EQ('0', testD.Get_val(1));
	EXPECT_EQ('1', testD.Get_val(2));
	EXPECT_EQ('0', testD.Get_val(3));
	EXPECT_EQ('1', testD.Get_val(4));
	EXPECT_EQ('0', testD.Get_val(5));
	EXPECT_EQ('1', testD.Get_val(6));
	EXPECT_EQ('0', testD.Get_val(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ('1', testD.Get_val(8));
}

TEST(TestClassDiag, TestCopy_1)
{
	char str[9] = "10110100";
	time_diag testD(str);

	EXPECT_ANY_THROW(testD.copy(-2));
	EXPECT_ANY_THROW(testD.copy(8));
	EXPECT_ANY_THROW(testD.copy(7));
}

TEST(TestClassDiag, TestShR)
{
	char str[12] = "10110100110";
	time_diag testD(str);

	testD.shift_right(5);

	EXPECT_EQ(16, testD.Get_b_d());
	EXPECT_EQ(9, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(40, testD.Get_max_s());
	EXPECT_EQ(5, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(1, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(1, testD.Get_dur(4));
	EXPECT_EQ(1, testD.Get_dur(5));
	EXPECT_EQ(2, testD.Get_dur(6));
	EXPECT_EQ(2, testD.Get_dur(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ('x', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('0', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	EXPECT_EQ('1', testD.Get_val(5));
	EXPECT_EQ('0', testD.Get_val(6));
	EXPECT_EQ('1', testD.Get_val(7));
	EXPECT_EQ('0', testD.Get_val(8));

}

TEST(TestClassDiag, TestShR_1)
{
	char str[12] = "10110100110";
	time_diag testD(str);
	char str1[41] = "1010101010101010101010101010101010101010";
	time_diag testD1(str1);

	EXPECT_ANY_THROW(testD.shift_right(-2));
	EXPECT_ANY_THROW(testD.copy(52));
	EXPECT_ANY_THROW(testD.copy(5));
}

TEST(TestClassDiag, TestShL)
{
	char str[12] = "10110100110";
	time_diag testD(str);
	time_diag testD_;

	testD_= testD.shift_left(5);

	EXPECT_EQ(6, testD_.Get_b_d());
	EXPECT_EQ(4, testD_.Get_b_v());
	EXPECT_EQ(60, testD_.Get_max_d());
	EXPECT_EQ(40, testD_.Get_max_s());
	EXPECT_EQ(1, testD_.Get_dur(0));
	EXPECT_EQ(2, testD_.Get_dur(1));
	EXPECT_EQ(2, testD_.Get_dur(2));
	EXPECT_EQ(1, testD_.Get_dur(3));
	
	EXPECT_EQ('1', testD_.Get_val(0));
	EXPECT_EQ('0', testD_.Get_val(1));
	EXPECT_EQ('1', testD_.Get_val(2));
	EXPECT_EQ('0', testD_.Get_val(3));
	
}

TEST(TestClassDiag, TestShL_1)
{
	char str[12] = "10110100110";
	time_diag testD(str);
	time_diag testD_;

	EXPECT_ANY_THROW(testD_=testD.shift_left(-2));
	EXPECT_ANY_THROW(testD_=testD.shift_left(62));
}

TEST(TestClassDiag, TestInput)
{
	time_diag testD;
	char *str = "01\n11\nx1\nr1\nt1\n61\nr1\n11\n11\n01\n01\n11\n01\n11\n01\nx1\n11\n";
	istringstream ss(str);
	ss >> testD;
	EXPECT_EQ(17, testD.Get_b_d());
	EXPECT_EQ(11, testD.Get_b_v());
	EXPECT_EQ(60, testD.Get_max_d());
	EXPECT_EQ(1, testD.Get_dur(0));
	EXPECT_EQ(1, testD.Get_dur(1));
	EXPECT_EQ(5, testD.Get_dur(2));
	EXPECT_EQ(2, testD.Get_dur(3));
	EXPECT_EQ(2, testD.Get_dur(4));
	EXPECT_EQ(1, testD.Get_dur(5));
	EXPECT_EQ(1, testD.Get_dur(6));
	EXPECT_EQ(1, testD.Get_dur(7));
	EXPECT_EQ(1, testD.Get_dur(8));
	EXPECT_EQ(1, testD.Get_dur(9));
	EXPECT_EQ(1, testD.Get_dur(10));
	EXPECT_EQ('0', testD.Get_val(0));
	EXPECT_EQ('1', testD.Get_val(1));
	EXPECT_EQ('x', testD.Get_val(2));
	EXPECT_EQ('1', testD.Get_val(3));
	EXPECT_EQ('0', testD.Get_val(4));
	EXPECT_EQ('1', testD.Get_val(5));
	EXPECT_EQ('0', testD.Get_val(6));
	EXPECT_EQ('1', testD.Get_val(7));
	EXPECT_EQ('0', testD.Get_val(8));
	EXPECT_EQ('x', testD.Get_val(9));
	EXPECT_EQ('1', testD.Get_val(10));
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

